# Modification Toufic 
# Licensed under the MIT License.
# Docker image Alpine 3.2 avec powershell, ansible, rundeck, winrm et PowerCLI

#docker build -t admintoolz .
#docker run -p 4440:4440 -v d:\docker\rundeck:/home/rundeck/data/ -it --name adminpowercliansiblepwsh admintoolz
#docker run --rm -it --name adminpowercliansiblepwsh -p 4440:4440 -v d:\docker\rundeck:/home/rundeck/data/ admintoolz pwsh
#cree un fichier inventory.ini dans d:\docker\rundeck et mettre le nom genere du conteneur dans le fichier hosts 127.0.0.1 id du conteneur 

FROM alpine:3.12 AS installer-env

# Define Args for the needed to add the package
ARG PS_VERSION=7.0.0
ARG PS_PACKAGE=powershell-${PS_VERSION}-linux-alpine-x64.tar.gz
ARG PS_PACKAGE_URL=https://github.com/PowerShell/PowerShell/releases/download/v${PS_VERSION}/${PS_PACKAGE}
ARG PS_INSTALL_VERSION=7


ENV ANSIBLE_HOST_KEY_CHECKING=false
ENV PROJECT_BASE=RDECK_BASE/projects/Test-Project


# Download the Linux tar.gz and save it
ADD ${PS_PACKAGE_URL} /tmp/linux.tar.gz

# define the folder we will be installing PowerShell to
ENV PS_INSTALL_FOLDER=/opt/microsoft/powershell/$PS_INSTALL_VERSION

# Create the install folder
RUN mkdir -p ${PS_INSTALL_FOLDER}

# Unzip the Linux tar.gz
RUN tar zxf /tmp/linux.tar.gz -C ${PS_INSTALL_FOLDER} -v

# Start a new stage so we lose all the tar.gz layers from the final image
FROM alpine:3.12.1

# Copy only the files we need from the previous stage
COPY --from=installer-env ["/opt/microsoft/powershell", "/opt/microsoft/powershell"]

# Define Args and Env needed to create links
ARG PS_INSTALL_VERSION=7
ENV PS_INSTALL_FOLDER=/opt/microsoft/powershell/$PS_INSTALL_VERSION \
    \
    # Define ENVs for Localization/Globalization
    DOTNET_SYSTEM_GLOBALIZATION_INVARIANT=false \
    LC_ALL=en_US.UTF-8 \
    LANG=en_US.UTF-8 \
    # set a fixed location for the Module analysis cache
    PSModuleAnalysisCachePath=/var/cache/microsoft/powershell/PSModuleAnalysisCache/ModuleAnalysisCache \
    POWERSHELL_DISTRIBUTION_CHANNEL=PSDocker-Alpine-3.12

# Install dotnet dependencies and ca-certificates
RUN apk add --no-cache \
    ca-certificates \
    less \
    \
    # PSReadline/console dependencies
    ncurses-terminfo-base \
    \
    # .NET Core dependencies
    krb5-libs \
    libgcc \
    libintl \
    libssl1.1 \
    libstdc++ \
    tzdata \
    userspace-rcu \
    zlib \
    icu-libs \
	sudo  \
	python3 \
	py3-pip \
	openjdk8-jre \
	bash \
	py-pip \
	openssl \
	alpine-sdk \
	python3-dev \
	libffi-dev \
	openssh-client \
	curl \
	openssl-dev \
	build-base && \
    pip install --upgrade pip cffi && \
    pip install ansible && \
	pip install pyvmomi && \
    pip install --upgrade pycrypto pywinrm[credssp] && \
    pip --no-cache-dir install azure-cli && \
	apk --update add sshpass openssh-client rsync  && \
	mkdir -p /etc/ansible && \
    echo 'localhost' > /etc/ansible/hosts \
    apk -X https://dl-cdn.alpinelinux.org/alpine/edge/main add --no-cache \
    lttng-ust \
    \
    # PowerShell remoting over SSH dependencies
    openssh-client \
    \
    # Create the pwsh symbolic link that points to powershell
    && ln -s ${PS_INSTALL_FOLDER}/pwsh /usr/bin/pwsh \
    \
    # Give all user execute permissions and remove write permissions for others
    && chmod a+x,o-w ${PS_INSTALL_FOLDER}/pwsh \
    # intialize powershell module cache
    # and disable telemetry
    && echo "Creating Rundeck user and group..." && addgroup rundeck && adduser -h /home/rundeck -D -s /bin/bash -G rundeck rundeck \
	&& export POWERSHELL_TELEMETRY_OPTOUT=1 \
    && pwsh \
        -NoLogo \
        -NoProfile \
        -Command " \
          \$ErrorActionPreference = 'Stop' ; \
          \$ProgressPreference = 'SilentlyContinue' ; \
          while(!(Test-Path -Path \$env:PSModuleAnalysisCachePath)) {  \
            Write-Host "'Waiting for $env:PSModuleAnalysisCachePath'" ; \
            Start-Sleep -Seconds 6 ; \
          }" 

# Install PowerCLI  
RUN   pwsh -c install-module VMware.PowerCLI -Force 

EXPOSE 4440 4443

#Install Rundeck

RUN   mkdir -p  /home/rundeck 
COPY   --chmod=755 run.sh /home/rundeck
RUN     mkdir -p /var/lib/rundeck/.ssh
RUN     mkdir -p /home/rundeck/ssl 
RUN     mkdir -p /home/rundeck/data
RUN     mkdir -p /home/rundeck/projects/Test-Project
COPY     rundeck-3.3.9-20210201.war /home/rundeck/rundeck.war
COPY --chown=rundeck:rundeck project.properties /home/rundeck/projects/Test-Project/etc/
COPY --chown=rundeck:rundeck ansible-plugin-*.jar /home/rundeck/projects/Test-Project/libext/
COPY --chmod=600 rundeckkey_rsa* /home/rundeck/
RUN ansible-galaxy collection install community.windows
RUN ansible-galaxy collection install community.docker
RUN pip install docker
RUN ansible-galaxy collection install community.kubernetes
RUN ansible-galaxy collection install community.mysql
RUN pip install PyMySQL
RUN ansible-galaxy collection install community.postgresql
RUN ansible-galaxy collection install community.vmware
RUN pip install --upgrade pip setuptools
#RUN pip install --upgrade git+https://github.com/vmware/vsphere-automation-sdk-python.git


#Pour lancer un powershell
CMD [ "pwsh" ]
#Pour lancer rundeck
#CMD     bash /home/rundeck/run.sh



